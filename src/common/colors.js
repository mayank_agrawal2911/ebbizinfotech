/**
 * set of all colors given above
 */
const COLORS = {
  green: '#43B028',
  inputText: '#6D6B6B',
  whiteshade: '#E7E7E7',
  darkBlack: '#0B0909',
  white: '#FFFFFF',
  grayText: '#6D6B6B',
  lightblack: '#0B0909',
  grayshade2: '#F5F5F5',
  lightgray: '#9D9D9D',
};

export default COLORS;
