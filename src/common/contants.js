import { Platform, Dimensions } from 'react-native';
const isIOS = Platform.OS === 'ios';
const isAndroid = Platform.OS === 'android';
const { width, height } = Dimensions.get('window');
const isLandscape = width > height;
const isPortrait = width < height;
const screenWidth = width;
const screenHeight = height;

const common = {
   isLandscape,
   isPortrait,
   screenWidth,
   screenHeight,
   isIOS,
   isAndroid,

}

/**constants used in app */
const CONSTANTS = {
   ...common,
}

export default CONSTANTS;