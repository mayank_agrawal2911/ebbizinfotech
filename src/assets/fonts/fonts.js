const FONTS = {
  poppinsbold: 'Poppins-Bold',
  poppinsMedium: 'Poppins-Medium',
  poppinsRegular: 'Poppins-Regular',
};

export default FONTS;
