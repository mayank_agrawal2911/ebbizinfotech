import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  RefreshControl,
} from 'react-native';
import axios from 'axios';
import COLORS from '../common/colors';
import {scale} from '../common/Scale';
import {FONTS} from '../assets';
const NewsScreenViewController = ({route, navigation}) => {
  const [newsData, setnewsData] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    getnewsData();
  }, [navigation]);

  const getnewsData = async () => {
    try {
      const response = await axios.get(
        'https://newsapi.org/v2/top-headlines?country=in&apiKey=9f97868125ae4ec19869ed8aa1300418',
      );
      console.log('response', response);
      setnewsData(response.data.articles);
      setRefreshing(false);
    } catch (error) {
      setRefreshing(false);
    }
  };

  const onRefresh = () => {
    setRefreshing(true);
    getnewsData();
  };
 
  const rendernewsData = ({item}) => (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('NewsDetailViewController', {
          title: item.title,
          description: item.description,
          publishedAt: new Date(item.publishedAt).toLocaleDateString(),
          urlToImage: item.urlToImage,
        })
      }>
      <View style={styles.newsContainer}>
        <Image style={styles.newsImage} source={{uri: item.urlToImage}} />
        <View style={styles.newsDetailsView}>
          <Text style={styles.newsTitle}>{item.title}</Text>
          <Text style={styles.newsDate}>
            {new Date(item.publishedAt).toLocaleDateString()}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.headerTitle}>News</Text>
      </View>
      <View style={styles.Subcontainer}>
        <FlatList
          data={newsData}
          renderItem={rendernewsData}
          keyExtractor={item => item.id}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  Subcontainer: {
    backgroundColor: COLORS.white,
  },
  headerTitle: {
    fontSize: scale(30),
    marginLeft: scale(40),
    paddingVertical: scale(25),
    fontFamily: FONTS.poppinsMedium,
  },
  newsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: scale(20),
    marginVertical: scale(10),
    borderWidth: 1,
    borderRadius: scale(10),
    paddingVertical:scale(10),
    borderColor: COLORS.lightgray,
  },
  newsDate: {
    fontSize: scale(14),
    color: COLORS.grayText,
    alignSelf: 'flex-end',
  },
  newsImage: {
    width: scale(50),
    alignSelf: 'center',
    height: scale(50),
    marginHorizontal:10,
  },
  newsDetailsView: {
    flex: 1,
    marginHorizontal: scale(10),
  },
  newsTitle: {
    fontSize: scale(14),
    fontFamily: FONTS.poppinsMedium,
  },
  newsChannelName: {
    fontSize: scale(14),
    color: COLORS.grayText,
  },
  seperator: {
    borderWidth: 1,
    borderColor: COLORS.lightgray,
    marginVertical: scale(10),
  },
});

export default NewsScreenViewController;
