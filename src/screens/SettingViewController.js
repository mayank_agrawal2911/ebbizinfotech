import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {FONTS} from '../assets';
import COLORS from '../common/colors';
import {scale} from '../common/Scale';
const SettingsScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.SettingStyle}>Settings</Text>
      <View style={styles.Subcontainer}>
        <ScrollView style={styles.Subcontainer} bounces={false} showsVerticalScrollIndicator={false}>
          <View style={styles.subHeader}>
            <View style={styles.AccountView}>
              <Text style={styles.accountText}>Edit Profile</Text>
            </View>
            <View style={styles.AccountView}>
              <Text style={styles.accountText}>Language</Text>
            </View>
            <View style={styles.AccountView}>
              <Text style={styles.accountText}>T & C Policy</Text>
            </View>
            <View style={styles.AccountView}>
              <Text style={styles.accountText}>About Us</Text>
            </View>
            <View style={styles.AccountView}>
              <Text style={styles.accountText}>Contact Us</Text>
            </View>
            <View style={styles.AccountView}>
              <Text style={styles.accountText}>Logout</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  subContainer: {
    marginHorizontal: scale(20),
  },
  SettingStyle: {
    fontSize: scale(24),
    marginLeft: scale(40),
    paddingVertical: scale(25),
    fontFamily: FONTS.poppinsMedium,
  },
  Subcontainer: {
    flex: 1,
    backgroundColor: COLORS.white,
    shadowColor: '#000000',
    shadowOpacity: 0.4,
    shadowRadius: 5,
    borderTopLeftRadius: scale(40),
    borderTopRightRadius: scale(40),
  },
  headerTitle: {
    fontSize: scale(28),
    color: COLORS.darkBlack,
    fontFamily: FONTS.poppinsbold,
  },
  subHeader: {
    marginVertical: scale(10),
    marginHorizontal: scale(10),
  },
  sectionTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  AccountView: {
    backgroundColor: COLORS.grayshade2,
    padding: scale(10),
    paddingVertical: scale(15),
    marginVertical: scale(10),
    borderRadius: scale(10),
  },
  accountText: {
    fontSize: 16,
  },
});

export default SettingsScreen;
