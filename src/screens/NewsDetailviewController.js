import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {FONTS} from '../assets';
import IMAGES from '../assets/image';
import COLORS from '../common/colors';
import {scale} from '../common/Scale';

const NewsDetailScreen = ({route, navigation}) => {
  const {title, description, publishedAt, urlToImage} = route.params;

  return (
    <View style={styles.container}>
      <View style={styles.newsDetailContainer}>
        <TouchableOpacity
          style={styles.backIconView}
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={IMAGES.backIcon}
            style={{height: scale(20), width: scale(20)}}
          />
        </TouchableOpacity>
        <Image
          source={{uri: urlToImage}}
          resizeMode={'cover'}
          style={styles.newsImage}
        />
        <ScrollView
          style={{paddingHorizontal: scale(10)}}
          showsVerticalScrollIndicator={false}>
          <Text style={styles.newsDate}>Published At :{publishedAt}</Text>
          <Text style={styles.newsTitle}>{title}</Text>
          <Text style={styles.newsDescription}>{description}</Text>
        </ScrollView>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  newsDetailContainer: {},
  newsImage: {
    height: scale(250),
  },
  backIconView: {
    position: 'absolute',
    zIndex: 1,
    marginTop: scale(10),
    height: scale(30),
    justifyContent:'center',
    alignItems:'center',
    width: scale(30),
    borderRadius:scale(15),
    backgroundColor: 'white',
    marginHorizontal: scale(20),
  },
  newsDate: {
    alignSelf: 'flex-start',
    paddingVertical: scale(10),
    color: COLORS.grayText,
    fontSize: scale(14),
    fontFamily: FONTS.poppinsMedium,
  },
  newsTitle: {
    fontSize: scale(20),
    fontFamily: FONTS.poppinsMedium,
  },
  newsDescription: {
    fontSize: scale(16),
    color: COLORS.grayText,
    fontFamily: FONTS.poppinsRegular,
  },
});
export default NewsDetailScreen;
