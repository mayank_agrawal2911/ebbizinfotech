import * as React from 'react';
import {Image, SafeAreaView, StatusBar, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {FONTS} from '../assets';
import COLORS from '../common/colors';
import IMAGES from '../assets/image';
import {scale} from '../common/Scale';
import NewsScreenViewController from '../screens/NewsScreenViewController';
import NewsDetailViewController from '../screens/NewsDetailviewController';
import SettingsScreen from '../screens/SettingViewController';

const BottomTab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
const NewsStack = createNativeStackNavigator();

function NewstackApp() {
  return (
    <NewsStack.Navigator>
      <NewsStack.Screen
        name="NewsViewController"
        options={{headerShown: false}}
        component={NewsScreenViewController}
      />
      <NewsStack.Screen
        name="NewsDetailViewController"
        options={{headerShown: false}}
        component={NewsDetailViewController}
      />
    </NewsStack.Navigator>
  );
}

const BottomTabNavigator = () => {
  return (
    <BottomTab.Navigator
      screenOptions={({route}) => ({
        gestureEnabled: true,
        tabBarActiveTintColor: COLORS.green,
        tabBarInactiveTintColor: COLORS.inputText,
        tabBarLabelStyle: {
          fontSize: scale(12),
          fontWeight: '300',
          paddingTop: scale(10),
          fontFamily: FONTS.poppinsRegular,
        },
        tabBarStyle: {padding: scale(15)},

        unmountOnBlur: false,
        tabBarIcon: ({focused}) => {
          const {name} = route;
          if (name === 'News') {
            return (
              <Image
                style={{
                  height: scale(20),
                  width: scale(17),
                  resizeMode: 'cover',
                  tintColor: focused ? COLORS.green : COLORS.inputText,
                }}
                source={focused ? IMAGES.earningImage : IMAGES.earningImage}
              />
            );
          } else if (name === 'Setting') {
            return (
              <Image
                style={{
                  height: scale(20),
                  width: scale(17),
                  resizeMode: 'cover',
                  tintColor: focused ? COLORS.green : COLORS.inputText,
                }}
                source={focused ? IMAGES.myorder : IMAGES.myorder}
              />
            );
          }
        },
      })}>
      <BottomTab.Screen
        name="News"
        component={NewstackApp}
        options={{headerShown: false}}
      />
      <BottomTab.Screen
        name="Setting"
        component={SettingsScreen}
        options={{headerShown: false}}
      />
    </BottomTab.Navigator>
  );
};

export const Navigation = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <NavigationContainer>
        <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
        <Stack.Navigator>
          <Stack.Screen
            name="BottomTabNavigator"
            options={{headerShown: false}}
            component={BottomTabNavigator}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
};

export default Navigation;
